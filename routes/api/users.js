const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const passport = require("passport");

// Load input validation
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");
const sendMail = require("../../validation/forgotpass");
const resetpassword = require("../../validation/resetPass")
const validateInsertInput = require("../../validation/addemployee")
// Loading required models
const User = require("../../models/User");
const Employee = require("../../models/Employee");

let storedEmail = ""
// @route POST api/users/register
// @desc Register user
// @access Public
router.post("/register", (req, res) => {
  // Form validation

  const { errors, isValid } = validateRegisterInput(req.body);

  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      return res.status(400).json({ email: "Email already exists" });
    } else {
      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
      });

      // Hash password before saving in database
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then(user => res.json(user))
            .catch(err => console.log(err));
        });
      });
    }
  });
});

router.post("/displayData", async (req, res) => {
  console.log('in Display')
  resultArray = []
  resultArray = await Employee.find();
  console.log(Array.isArray(resultArray))
  res.json({
    resultArray
  })
})

router.post("/login", (req, res) => {
  // Form validation

  const { errors, isValid } = validateLoginInput(req.body);

  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  // Find user by email
  User.findOne({ email }).then(user => {
    // Check if user exists
    if (!user) {
      return res.status(404).json({ emailnotfound: "Email not found" });
    }

    // Check password
    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        // User matched
        // Create JWT Payload
        const payload = {
          id: user.id,
          name: user.name
        };

        // Sign token
        jwt.sign(
          payload,
          keys.secretOrKey,
          {
            expiresIn: 31556926 // 1 year in seconds
          },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token
            });
          }
        );
      } else {
        return res
          .status(400)
          .json({ passwordincorrect: "Password incorrect" });
      }
    });
  });
});

router.post("/forgetPassword", (req, res) =>{

  const { errors, isValid } = sendMail(req.body);
  const email = req.body.email;
  
  storedEmail = email;
  console.log(storedEmail)

  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  
  User.findOne({ email }).then(user => {
    // Check if user exists
    if (!user) {
      return res.status(404).json({ emailnotfound: "Email not found" });
    }
    else{
      console.log("Check Your Mail!")
    }
  })
})

router.post("/ResetPassword", (req, res) =>{

  const { errors, isValid } = resetpassword(req.body)
  const newPassword = req.body.password
  const email = storedEmail
  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  
  User.findOne({ email }).then(user => {

    if(!user)
    {
      return res.status(404).json({ emailnotfound: "User not found" });
    }
    else{
      bcrypt.hash(newPassword, 10).then(hash=>{
        user.password = hash
        user.save().then((saveduser)=>{
          console.log("Password Update Success!")
          res.json({success: "Password update success!"})
        })
      })
    }    
  })
})

router.post("/addEmployee", (req, res) => {
  // Form validation
  console.log('addEmployee in')
  
  const { errors, isValid } = validateInsertInput(req.body);

  console.log('after const')
  
  if (!isValid) {
    return res.status(400).json(errors);
  }

  Employee.findOne({ email: req.body.email }).then(emp => {
    if (emp) {
      return res.status(400).json({ email: "Email already exists" });
    } else {
      const newEmp = new Employee({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        phone: req.body.phone,
        Designation: req.body.Designation
      });
      newEmp
        .save()
        .then(emp => res.json(emp))
        .catch(err => console.log(err));
    }
  });
});

module.exports = router;
