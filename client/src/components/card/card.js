import React from 'react'
import Calender from "react-calendar";
import cardImg from "../../images/cardImg.jpeg";
import ProfileCard from "../layout/ProfileCard/ProfileCard"
//import 'react-calendar/dist/Calendar.css';
import './calender.css'
import Grid from '@material-ui/core/Grid';
import Item from '@material-ui/core/ListItem'

function card() {
    return (
        // <div class="row">
        //     <div class="col s2 pull-s0" style={{marginLeft:'-50px'}}>
        //       <div class="card">
        //         <div class="card-image waves-effect waves-block waves-light">
        //           <img class="activator" src={cardImg}/>
        //         </div>
        //         <div class="card-content">
        //           <span class="card-title activator grey-text text-darken-4"><b>Card Title</b><i class="material-icons right">more_vert</i></span>
        //         </div>
        //         <div class="card-reveal">
        //           <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
        //           <p>Here is some more information about this product that is only revealed once clicked on.</p>
        //         </div>
        //       </div>
        //     </div>
        //     <div class="col s3 push-s9"><Calender/></div>
        // </div>
      <div>
        <Grid container spacing={1} justifyContent="center">
          <Grid item xs={3} style={{ marginLeft: '20px', marginTop: '10px'}}>
            <Item>
              <ProfileCard/>
            </Item>
          </Grid>
          <Grid item xs={2}>
            <Item>
              <div class="card">
                <div class="card-image waves-effect waves-block waves-light">
                  <img class="activator" src={cardImg}/>
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4"><b>Card Title</b><i class="material-icons right">more_vert</i></span>
                </div>
                <div class="card-reveal">
                  <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                  <p>Here is some more information about this product that is only revealed once clicked on.</p>
                </div>
              </div>
            </Item>
          </Grid>
          <Grid item xs={3} style={{marginTop:'10px'}}>
            <Item>
            <Calender className="c1"/>    
            </Item>
          </Grid>
        </Grid>
      </div>
    )
}

export default card
