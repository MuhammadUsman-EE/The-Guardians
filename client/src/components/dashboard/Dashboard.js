import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import Navbar from "../layout/Navbar";
import Todo from "./todo"
import 'react-calendar/dist/Calendar.css';
// import minCard from "../layout/card/card";
import Card from "../card/card";
// import Sidebar from "../Sidebar/sidebar";
// import { Router,Route } from "express";
// import SideNav, { Toggle, Nav, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
// import Home from "../Home/Home";
// import e from 'express';

class Dashboard extends Component {
  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  render() {
    const { user } = this.props.auth;
    

    return ( 
      <div>
        <Navbar/>
        <Card/>      
        <Todo/>
      </div>
    );
  }
}

Dashboard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Dashboard);
