import React, { Component, useState } from "react";
import { Link, withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import classnames from "classnames";
import Navbar from "../layout/Navbar";
import Grid from '@material-ui/core/Grid';
import Item from '@material-ui/core/ListItem'
import DatePickers from "./datetextbox"
import TextField from '@mui/material/TextField';
import exitimg from "../../images/exit.svg"

class initiateClearance extends Component{
    constructor(){
        super();
        this.setState={
            email:"",
            errors:{}
        };
    }

    render(){
        
        return(
            <div>
                <header><Navbar/></header>
                <Grid container spacing={1} >
                    <Grid item xs={4} style={{ marginLeft:'300px'}}>
                        <Item>
                            <h4><b>Resignation Procedure</b></h4>
                        </Item>
                        <Item>
                            <form noValidate onSubmit={this.onSubmit}>
                                <div class="input-field col s12" style={{width:"363px"}}>
                                    <input
                                    onChange={this.onChange}
                                    // value={this.state.email}
                                    // error={errors.email}
                                    id="email"
                                    type="email"
                                    // class={classnames("", {
                                    //     invalid: errors.email || errors.emailnotfound
                                    // })}
                                    />
                                    <label htmlFor="email" >Email</label>
                                    {/* <span class="red-text">
                                    {errors.email}
                                    {errors.emailnotfound}
                                    </span> */}
                                </div>
                            </form>
                        </Item>
                        <Item>
                            <div class="input-field col s12" style={{width:"363px"}}>
                                <input
                                onChange={this.onChange}
                                // value={this.state.email}
                                // error={errors.email}
                                id="Designation"
                                type="text"
                                // class={classnames("", {
                                //     invalid: errors.email || errors.emailnotfound
                                // })}
                                />
                                <label htmlFor="Designation" >Designation</label>
                                {/* <span class="red-text">
                                {errors.email}
                                {errors.emailnotfound}
                                </span> */}
                            </div>
                        </Item>
                        <Item>
                            <div class="input-field col s12" style={{width:"363px"}}>
                                <input
                                onChange={this.onChange}
                                // value={this.state.email}
                                // error={errors.email}
                                id="Department"
                                type="text"
                                // class={classnames("", {
                                //     invalid: errors.email || errors.emailnotfound
                                // })}
                                />
                                <label htmlFor="Department" >Department</label>
                                {/* <span class="red-text">
                                {errors.email}
                                {errors.emailnotfound}
                                </span> */}
                            </div>
                        </Item>
                        <Item><h5>Date of relieving</h5></Item>
                        <Item>
                            <DatePickers/>    
                        </Item>
                        <Item><h5>Reason for Resignation</h5></Item>
                        <Item>
                            <TextField id = "standard-multiline-static"
                            label="Enter here..."
                            multiline
                            rows={3}
                            style={{width:"363px"}}
                            />  
                        </Item>
                    </Grid>
                    <Grid item xs={5}>
                        <Item style={{marginTop:'130px'}}>
                            <img class="responsive-img" src={exitimg} alt="Resignation"/>
                        </Item>
                    </Grid>
                </Grid>
            </div>
            
        )
    }
}
export default initiateClearance