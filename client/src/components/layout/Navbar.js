// import React, { Component } from "react";
// import { Link } from "react-router-dom";

// class Navbar extends Component {
//   render() {
//     return (
//       <div className="navbar-fixed" style={{}}>
//         <nav className="z-depth-0" >
//           <div className="nav-wrapper white">
//             <Link
//               to="/"
//               style={{
//                 fontFamily: "monospace",
  
//               }}
//               className="col s5 brand-logo center black-text"
//             >
//               <i className="material-icons"></i>
//               The GuardiaNs
//             </Link>
//           </div>
//         </nav>
//       </div>
//     );
//   }
// }

// export default Navbar;
import React, { useState } from 'react';
import { connect } from "react-redux";
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import { SidebarData } from './SidebarData';
import './Navbar.css';
import { Link } from 'react-router-dom';
import { IconContext } from 'react-icons';
import {logoutUser} from '../../actions/authActions';
import setAuthToken from "../../utils/setAuthToken";
import PropTypes from "prop-types";

function Navbar() {
  const [sidebar, setSidebar] = useState(true);

  const showSidebar = () => setSidebar(!sidebar);

  const onLogoutClick = e =>{
    localStorage.removeItem('jwtToken');
    setAuthToken(false);
    window.location.href = "/login";
  };

  return (
    <>
      <IconContext.Provider value={{ color: '#fff' }}>
        <nav>
          <div class="nav-wrapper purple darken-1">
            <Link to='#' className='menu-bars'>
              <FaIcons.FaBars onClick={showSidebar} />
            </Link>
            <a href="#" class="brand-logo center">Guardians</a>
            <ul class="right hide-on-med-and-down">
              <li style={{marginRight:'5px'}}><a onClick={onLogoutClick}><i class="material-icons">logout</i></a></li>
            </ul>
          </div>
        </nav>
        <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
          <div class = "nav-wrapper">
            <ul className='nav-menu-items' onClick={showSidebar}>
              <li className='navbar-toggle'>
                <Link to='#' className='menu-bars' style={{marginLeft:'2px'}}>
                  <AiIcons.AiOutlineClose />
                </Link>
              </li>
              {SidebarData.map((item, index) => {
                return (
                  <li  key={index} className={item.cName}>
                    <Link to={item.path}>
                      {item.icon}
                      <span className="title">{item.title}</span>
                    </Link>
                  </li>
                );
              })}
            </ul>
          </div>
        </nav>
      </IconContext.Provider>
    </>
  );
}

export default Navbar;