const Validator = require("validator");
const isEmpty = require("is-empty");

module.exports = function validateInsertInput(data) {
  let errors = {};

  // Convert empty fields to an empty string so we can use validator functions
  data.firstname = !isEmpty(data.firstname) ? data.firstname : "";
  data.lastname = !isEmpty(data.lastname) ? data.lastname : "";  
  data.email = !isEmpty(data.email) ? data.email : "";
  data.phone = !isEmpty(data.phone) ? data.phone : "";
  data.Designation = !isEmpty(data.Designation) ? data.Designation : "";

  // Name checks
  if (Validator.isEmpty(data.firstname)) {
    errors.firstname = "First Name field is required";
  }
  if (Validator.isEmpty(data.lastname)) {
    errors.lastname = "Last Name field is required";
  }
  

  // Email checks
  if (Validator.isEmpty(data.email)) {
    errors.email = "Email field is required";
  } else if (!Validator.isEmail(data.email)) {
    errors.email = "Email is invalid";
  }

  //Phone checks
  if (Validator.isEmpty(data.phone)) {
    errors.phone = "Phone Number is required";
  }

  //Designation checks
  if (Validator.isEmpty(data.Designation)) {
    errors.Designation = "Designation is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
